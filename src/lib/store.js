import {configureStore} from '@reduxjs/toolkit'
import hwatuReducer from "@/lib/features/hwatu/hwatuSlice";

export default configureStore({
    reducer: {
        hwatu: hwatuReducer
    },
})
