import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    hwatus: [
        { name: '1월', imgUrl: '1.png', means: ['소식,아기'] },
        { name: '2월', imgUrl: '2.png', means: ['애인,이성'] },
        { name: '3월', imgUrl: '3.png', means: ['만남,데이트,산책'] },
        { name: '4월', imgUrl: '4.png', means: ['구설수'] },
        { name: '5월', imgUrl: '5.png', means: ['음식,국수'] },
        { name: '6월', imgUrl: '6.png', means: ['길조,기쁨'] },
        { name: '7월', imgUrl: '7.png', means: ['횡재,돈,행운'] },
        { name: '8월', imgUrl: '8.png', means: ['달밤,밤,어둠'] },
        { name: '9월', imgUrl: '9.png', means: ['술'] },
        { name: '10월', imgUrl: '10.png', means: ['근심,걱정'] },
        { name: '11월', imgUrl: '11.png', means: ['돈,금전'] },
        { name: '12월', imgUrl: '12.png', means: ['손님'] },
    ],
    mixHwatus: [],
    pickHwatus: [],
}

const hwatuSlice = createSlice({
    name: 'hwatu',
    initialState,
    reducers: {
        resetHwatu: (state) => {
            state.mixHwatus = []
            state.pickHwatus = []
        },
        mixHwatu: (state) => {
            state.mixHwatus = [...state.hwatus].map(hwatu => ({...hwatu, selected: false })).sort(() => Math.random() - 0.5)
        },
        pickHwatu: (state, action) => {
            const hwatuIndex = state.mixHwatus.findIndex(hwatu => hwatu.name === action.payload)
            if (hwatuIndex !== -1 && !state.mixHwatus[hwatuIndex].selected) {
                state.mixHwatus[hwatuIndex].selected = true
                state.pickHwatus.push(state.mixHwatus[hwatuIndex])
            }
        },
    }
})

export const { resetHwatu, mixHwatu, pickHwatu } = hwatuSlice.actions
export default hwatuSlice.reducer