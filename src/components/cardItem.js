import React from "react";

export default function CardItem({ hwatu, handlePick }) {
    return (
        <div className='hw-to-in' key={hwatu.name} onClick={() => handlePick(hwatu.name)}>
            {hwatu.selected ? (
                <div>
                    <img className='boder-ra' src={`/images/${hwatu.imgUrl}`} alt={hwatu.name}/>
                    <p>{hwatu.name} {hwatu.means}</p>
                </div>
            ) : (
                <div>
                    <img className='boder-ra' src={`/images/back.png`} alt="back"/>
                </div>
            )}
        </div>
    )
}