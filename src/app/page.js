'use client'

import {useSelector, useDispatch} from "react-redux";
import {mixHwatu, pickHwatu, resetHwatu} from "@/lib/features/hwatu/hwatuSlice";
import axios from "axios"
import {useState} from "react";
import styles from "./globals.css"
import CardItem from "@/components/cardItem";

export default function Home() {
    const [response, setResponse] = useState('');
    const {mixHwatus, pickHwatus} = useSelector((state) => state.hwatu)
    const dispatch = useDispatch();

    const handleMix = () => {
        dispatch(mixHwatu())
    }

    const handlePick = (hwatuName) => {
        if (pickHwatus.length < 3) {
            dispatch(pickHwatu(hwatuName))
        } else {
            alert('최대 3장만 선택 할 수 있습니다.')
        }
    }

    const handleReset = () => {
        dispatch(resetHwatu())
        setResponse('')
    }

    const handleConfirm = () => {
        if (pickHwatus.length === 3) {
            const means = pickHwatus.flatMap(hwatu => hwatu.means).join(', ')
            const commend = `${means} 이라는 단어들을 가지고 오늘 하루의 운세를 이야기해줘 허구여도 상관없어 하지만 5줄로 풀어줬으면 좋겠어 구지 좋은말만 할 필요도 없고 나쁜말만 할 필요도 없어 마음 가는대로 말해줘`

            const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=AIzaSyBIyFKDg5lSdJWsIR3nR6-0AJmFv39TI7U'

            const data = {"contents": [{"parts": [{"text": commend}]}]}
            axios.post(apiUrl, data)
                .then(res => {
                    setResponse(res.data.candidates[0].content.parts[0].text)
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }

    return (
        <div className='body-centa'>
            <h1 className='text-4xl mt-7 font-bold'>화투점 3개를 뽑아라</h1>
            {mixHwatus.length === 0 ? (<button className='mt-7 focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800' onClick={handleMix}>시작</button>
            ) : (
                <div className='hw-to'>
                    {mixHwatus.map(hwatu => (
                        <CardItem hwatu={hwatu} handlePick={handlePick} />
                    ))}
                </div>
            )}
            <div className='body-centa'>
                {pickHwatus.length === 3 && (
                    <div>
                        <button className='hw-to-in text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800' onClick={handleConfirm}>운세보기</button>
                        <button className='hw-to-in text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800' onClick={handleReset}>다시하기</button>
                        <div>
                            {response && <p className='resp text-2xl'>{response}</p>}
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}
