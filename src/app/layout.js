'use client'

import {Provider} from "react-redux";
import store from "@/lib/store";

import {Inter} from "next/font/google";

const inter = Inter({subsets: ["latin"]});

export default function RootLayout({children}) {
    return (
        <html lang="en">
        <Provider store={store}>
            <body className={inter.className}>{children}</body>
        </Provider>
        </html>
    );
}
